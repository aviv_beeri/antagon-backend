# Antagon.net Backend Service

The JSON API backend server for the Antagon microblogging service.

## Overview

## Setup Development Environment

Important: This project requires **Node v6** to run. I recommend using [nvm](https://github.com/creationix/nvm) to install alternate versions of Node.

1. Clone this repository
2. `npm install` (using Node v6)
3. `npm start` (to transpile the project into `build/` and begin execution)

## Style Guidelines

The project is written using Typescript and ES6 (ES2015) features only.

The build system is integrated with both TSLint and ESLint, thanks to the `tslint.json` and `.eslintrc.json` files provided. When the service is started or reloads, all the code is run through both linters and you will see their output.

We intend to code in the [AirBnB Javascript Style](https://github.com/airbnb/javascript), and ESLint is configured to use their rules, with a few modifications to suit the context of Typescript code.

For maximum benefit, this project aims to embrace as much of Typescript as possible.
Therefore, TSLint will insist upon giving all variables type definitions.

## Type definitions
When installing new NPM modules, check if they have an `@types` declaration. This package/file is
provided by the community to give standard Javascript modules types from Typescript, allowing for
interoperability.
If you import a module which does not have one, you will have to create your own for Typescript to
detect the module. Examples of these can be found in "src/types".

You can create a simple stub declaration:
```
declare module 'module-name' {

}
```

You might end up declaring significant portions of the module's API, but this will ensure more
consistent code in the future.

More information about importing type definitions can be found [here](https://blogs.msdn.microsoft.com/typescript/2016/06/15/the-future-of-declaration-files/).
