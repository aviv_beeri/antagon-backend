import * as http from 'http';
import * as express from 'express';
import * as cors from 'cors';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';
import { ServerOptions } from './utils/interfaces';

export function startServer(options: ServerOptions): Promise<http.Server> {
  return new Promise((resolve, reject) => {
    const app: express.Application = express();

    // Enable Middleware
    app.use(cors({

    }));
    app.use(morgan('dev'));
    app.use(bodyParser.json());
    // body-parser
    // helmet

    // Configure services
    if (options.services) {
      options.services.forEach((manifest) => {
        manifest.service(app, manifest.repo);
      });
    }

    // Start the server
    const server: http.Server = app.listen(options.port, () => resolve(server));
  });
}
