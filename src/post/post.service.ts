import * as express from 'express';
import { Post } from './post.model';
import PostRepository from './post.repo';

import authRequired from '../utils/authRequired';

export default function PostService(app: express.Application, repo: PostRepository): void {
  // TODO: Assert that a repository exists
  app.get('/posts/', (req, res, next) => {
    repo.getAllPosts().then((posts: Post[]) => {
      res.status(200).send(posts);
    });
  });

  app.post('/posts/', authRequired(), (req, res, next) => {
    const post: Post = new Post(req.body);
    repo.createNewPost(post).then((newPost: Post) => {
      res.status(201).send(newPost);
    });
  });

  app.put('/posts/:id', authRequired(), (req, res, next) => {
    const post: Post = new Post(req.body);
    repo.updatePost(post).then((newPost: Post) => {
      res.status(200).send(newPost);
    });
  });
}
