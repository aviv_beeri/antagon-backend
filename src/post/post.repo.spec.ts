/* tslint:disable:no-any */
import 'jasmine';
import PostRepository from './post.repo';
import { Post } from './post.model';
import { Database } from '../utils/interfaces';
import * as admin from 'firebase-admin';

describe('Post Repository', () => {
  let repo: PostRepository;

  const fakePosts: Post[] = [
    new Post({
      body: 'Evil plan',
      timePosted: '2017-07-16T00:00:01Z',
      author: '2'
    }),
    new Post({
      body: 'Hello world',
      timePosted: '2017-07-16T00:00:00Z',
      author: '1'
    })
  ];

  const mockDatabase: Database = {
    ref(path?: string): admin.database.Reference {
      const ref: any = jasmine.createSpyObj('ref', [
        'once',
        'push',
        'set',
        'ref',
        'forEach',
        'child'
      ]);
      ref.set.and.callFake((post: Post) => {
        fakePosts.push(post);
        return Promise.resolve(post);
      });
      ref.key = 'newId';

      ref.ref.and.returnValue(ref);
      ref.child.and.returnValue(ref);
      const snapshot: any = jasmine.createSpyObj('arraySnapshot', ['forEach']);
      snapshot.forEach.and.callFake((func: (value: any) => void) => {
        fakePosts.forEach((post) => {
          const spy: any = jasmine.createSpyObj('snapshot', ['val']);
          spy.val.and.returnValue(post);
          func(spy);
        });
      });

      ref.push.and.returnValue(ref);
      ref.once.and.returnValue(Promise.resolve(snapshot));
      return ref;
    }
  };

  beforeEach(() => {
    repo = new PostRepository(mockDatabase);
  });

  it('should retrieve all posts from a database', (done) => {
    expect(repo.getAllPosts).toBeDefined();
    repo.getAllPosts()
      .then((results) => {
        expect(results.length).toBe(fakePosts.length);
        expect(results[0].body).toEqual('Hello world');
        expect(results[1].body).toEqual('Evil plan');
      })
      .then(done)
      .catch(done.fail);
  });

  it('should let users create new posts', (done) => {
    const originalLength: number = fakePosts.length;
    const newPost: Post = new Post({
      body: 'Genius idea!',
      timePosted: new Date().toISOString(),
      author: '1'
    });

    repo.getAllPosts()
      .then((results) => {
        expect(results.length).toBe(originalLength);
        expect(results[0].body).toEqual('Hello world');
        expect(results[1].body).toEqual('Evil plan');
        return repo.createNewPost(newPost);
      })
      .then(() => repo.getAllPosts())
      .then((results) => {
        expect(results.length).toBe(originalLength + 1);
        expect(results[0].body).toEqual('Genius idea!');
        expect(results[0].author).toBe('1');
        expect(results[0].id).toBe('newId');

        expect(results[1].body).toEqual('Hello world');
        expect(results[2].body).toEqual('Evil plan');
      })
      .then(done)
      .catch(done.fail);
  });

  it('should allow a user to add a rating', (done) => {
    const userId: string = '1';
    repo.getAllPosts()
      .then((results) => {
        const post: Post = results[0];
        expect(results[0].positiveRatings).not.toContain(userId);
        post.rate('positive', userId);
        return repo.updatePost(post);
      })
      .then(() => repo.getAllPosts())
      .then((results) => {
        expect(results[0].positiveRatings).toContain(userId);
      })
      .then(done)
      .catch(done.fail);
  });
});
