// Simple Post Class
export type UserId = string;

interface Ratings {
  positive?: UserId[];
  negative?: UserId[];
  medium?: UserId[];
}

export class Post {
  id?: string;
  body: string;
  timePosted: string;
  author: UserId;
  positiveRatings?: Set<UserId>;
  negativeRatings?: Set<UserId>;
  mediumRatings?: Set<UserId>;
  // comments: Comments
 /* tslint:disable-next-line: no-any */
  constructor(params: any) {
    Object.assign(this, params);
    const ratings: Ratings = params.ratings || {};
    this.positiveRatings = this.positiveRatings || new Set(ratings.positive || []);
    this.negativeRatings = this.negativeRatings || new Set(ratings.negative || []);
    this.mediumRatings = this.mediumRatings || new Set(ratings.medium || []);
  }

  unrate(userId: string): void {
    this.positiveRatings.delete(userId);
    this.negativeRatings.delete(userId);
    this.mediumRatings.delete(userId);
  }

  rate(mood: string, userId: string): void {
    switch (mood) {
      case 'positive':
        this.unrate(userId);
        this.positiveRatings.add(userId);
        break;
      case 'negative':
        this.unrate(userId);
        this.negativeRatings.add(userId);
        break;
      case 'medium':
        this.unrate(userId);
        this.mediumRatings.add(userId);
        break;
      default: throw new Error(`Invalid mood used for post with id ${this.id}`);
    }
  }
}
