import { Post } from './post.model';
import { Repository, Database } from '../utils/interfaces';

export default class PostRepository extends Repository {

  private database: Database;

  constructor(database: Database) {
    super();
    this.database = database;
  }

  getAllPosts(): Promise<Post[]> {
    return this.database.ref('posts').once('value')
    .then((snapshots) => {
      // Firebase pushes to collections and retrieves from collections as
      // an object with keys that are sorted by time.
      //
      // In order to return a sane dataset to the client, we have to build
      // an array sorted in memory.
      //
      // This approach does not scale well, as we have to rebuild the array
      // on each request.
      //
      // TODO(aviv): it should either cache the results, or force firebase to provide arrays
      // somehow.
      const result: Post[] = [];
      snapshots.forEach((childSnapshot: admin.database.DataSnapshot) => {
        result.push(new Post(childSnapshot.val()));
      });

      return result.reverse();
    });
  }

  createNewPost(post: Post): Promise<Post> {
    const newPost: Post = Object.assign({}, post);
    const newRef: admin.database.Reference = this.database.ref().child('posts').push();
    newPost.id = newRef.key;
    return newRef.set(newPost).then(() => newPost);
  }

  updatePost(post: Post): Promise<Post> {
    return this.database.ref(`posts/${post.id}`).set(post).then(() => post);
  }
}
