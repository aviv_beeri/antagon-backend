/* eslint no-param-reassign: "off" */
import * as express from 'express';
import * as admin from 'firebase-admin';

export default function authRequired(): express.Handler {
  return (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const idToken: string = req.get('authorization').substr(7);
    admin.auth().verifyIdToken(idToken)
      .then((decodedToken) => {
        const uid = decodedToken.uid;
        const authenticated: boolean = !!uid;
        if (authenticated) {
          req.user = uid;
          next();
        } else {
          next(new Error('Unauthorized'));
        }
      }).catch((error) => {
        next(new Error('Unauthorized'));
      });
  };
}
