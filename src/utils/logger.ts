import * as winston from 'winston';

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
  timestamp: true,
  handleExceptions: true,
  humanReadableUnhandledException: true
});

export default winston;
