/* tslint:disable:no-any */
import * as express from 'express';
import * as admin from 'firebase-admin';

export interface Service {
  (app: express.Application, options: any): void;
}

export interface Database {
  ref(path?: string): admin.database.Reference;
  child?(): admin.database.Query;
}

export class Repository {}

export interface ServiceManifest {
  service: Service;
  repo: Repository;
}

export interface ServerOptions {
  services?: ServiceManifest[];
  port: number;
}
