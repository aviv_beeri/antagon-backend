// Enable source-map support so we can find errors in TS files.
// Node doesn't do this natively.
require('source-map-support').install();
import * as path from 'path';
import { startServer } from './server';
import { ServerOptions } from './utils/interfaces';

import PostService from './post/post.service';
import PostRepository from './post/post.repo';

import UserService from './user/user.service';
import UserRepository from './user/user.repo';

import * as admin from 'firebase-admin';

// This needs to load first to initialise the logging configuration
import winston from './utils/logger';

/*
  If a promise does not handle a rejected promise, this default handler is called.
*/
process.on('unhandledRejection', (err: Error) => {
  winston.error('Unhandled Rejection:');
  winston.error(err.toString());
});

const serviceAccountPath: string = path.resolve(__dirname, '../reduced_firebase.json');
let serviceAccount: admin.ServiceAccount;
if (!process.env.PRIVATE_KEY) {
  serviceAccount = require(serviceAccountPath);
} else {
  serviceAccount = {
    privateKey: process.env.PRIVATE_KEY.replace(/\\n/g, '\n'),
    projectId: process.env.PROJECT_ID,
    clientEmail: process.env.CLIENT_EMAIL
  };
}

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://antagon-12b6b.firebaseio.com'
});

const options: ServerOptions = {
  port: process.env.PORT || 8080,
  services: [
    {
      service: PostService,
      repo: new PostRepository(admin.database())
    },
    {
      service: UserService,
      repo: new UserRepository(admin.database())
    }
  ]
};

startServer(options).then((server) => {
  winston.info('System is working');
});
