import { User } from './user.model';
import { Repository, Database } from '../utils/interfaces';

export default class UserRepository extends Repository {

  private database: Database;

  constructor(database: Database) {
    super();
    this.database = database;
  }

  getUser(id: string): Promise<User> {
    return this.database.ref('users').child(id).once('value')
      .then((user) => new User(user.val()));
  }

  createUser(user: User): Promise<User> {
    const newUser: User = Object.assign({}, user);
    return this.database.ref().child(`users/${user.id}`)
      .set(newUser).then(() => newUser);
  }

  getAllUsers(): any {
    return {};
  }
  deleteUser(id: string): any {
    return {};
  }
}
