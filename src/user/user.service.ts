import * as express from 'express';
import { User } from './user.model';
import UserRepository from './user.repo';

import authRequired from '../utils/authRequired';

export default function UserService(app: express.Application, repo: UserRepository): void {
  // TODO: Assert that a repository exists
  app.get('/users/', (req, res, next) => {
    repo.getAllUsers().then((users: User[]) => {
      // TODO (aviv): We need to filter out private info
      // otherwise the villains will be exposed!
      res.status(200).send(users);
    });
  });

  app.get('/users/:id', (req, res, next) => {
    repo.getUser(req.params.id).then((user: User) => {
      // TODO (aviv): We need to filter out private info
      // otherwise the villains will be exposed!
      res.status(200).send(new User(user).getPublic());
    });
  });

  app.put('/users/:id', authRequired(), (req, res, next) => {
    const user: User = new User(req.body);
    if (user.id !== req.params.id || req.params.id !== req.user) {
      res.sendStatus(422);
      return;
    }
    repo.createUser(user).then((newUser: User) => {
      res.status(201).send(newUser);
    });
  });

  app.delete('/users/:id', authRequired(), (req, res, next) => {
    if (req.params.id !== req.user) {
      res.sendStatus(422);
      return;
    }
    repo.deleteUser(req.user).then(() => {
      res.sendStatus(204);
    });
  });
}
