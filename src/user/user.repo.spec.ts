/* tslint:disable:no-any */
import 'jasmine';
import UserRepository from './user.repo';
import { User } from './user.model';
import { Database } from '../utils/interfaces';
import * as admin from 'firebase-admin';

describe('User Repository', () => {
  let repo: UserRepository;

  const testUsers: User[] = [
    new User({
      id: '1',
      name: 'Norman Osborne',
      alias: 'The Green Goblin',
      email: 'norman@oscorp.com'
    }),
    new User({
      id: '2',
      name: 'Lex Luthor',
      email: 'lex@lexcorp.io'
    }),
    new User({
      id: '3',
      name: 'Jack',
      alias: 'The Joker',
      email: 'axis@evil.com'
    })
  ];

  const mockDatabase: Database = {
    ref(path?: string): admin.database.Reference {
      const ref: any = jasmine.createSpyObj('ref', [
        'once',
        'push',
        'set',
        'ref',
        'forEach',
        'child'
      ]);
      ref.set.and.callFake((user: User) => {
        testUsers.push(user);
        return Promise.resolve(user);
      });
      ref.key = 'newId';

      ref.ref.and.returnValue(ref);
      ref.child.and.callFake((id: string) => {
        const childRef: any = jasmine.createSpyObj('childRef', ['once']);
        childRef.once.and.returnValue(Promise.resolve(testUsers.find((user) => user.id === id)));
        return childRef;
      });
      const snapshot: any = jasmine.createSpyObj('arraySnapshot', ['forEach']);
      snapshot.forEach.and.callFake((func: (value: any) => void) => {
        testUsers.forEach((user) => {
          const spy: any = jasmine.createSpyObj('snapshot', ['val']);
          spy.val.and.returnValue(user);
          func(spy);
        });
      });

      ref.push.and.returnValue(ref);
      ref.once.and.returnValue(Promise.resolve(snapshot));
      return ref;
    }
  };

  beforeEach(() => {
    repo = new UserRepository(mockDatabase);
  });

  it('should retrieve a single user from a database if they exist', (done) => {
    expect(repo.getUser).toBeDefined();
    repo.getUser('1')
      .then((result) => {
        expect(result.id).toEqual('1');
      })
      .then(done)
      .catch(done.fail);
  });

  xit('should allow a user to be created', (done) => {
    const newUser: User = new User({
      id: '4',
      name: 'Dr Octavius',
      alias: 'Otto Octopus',
      email: 'o.octavius@esc.edu'
    });

    repo.getAllUsers()
      .then((results: User[]) => {
        expect(results.length).toBe(testUsers.length);
        return repo.createUser(newUser);
      })
      .then(() => repo.getAllUsers())
      .then((results: User[]) => {
        expect(results.length).toBe(testUsers.length);
        expect(results[results.length - 1].alias).toEqual(newUser.alias);
      })
      .then(done)
      .catch(done.fail);
  });

  xit('should allow a user to be deleted', (done) => {
    repo.getAllUsers()
      .then((results: User[]) => {
        expect(results.length).toBe(testUsers.length);
        return repo.deleteUser(testUsers[0].id);
      })
      .then(() => repo.getAllUsers())
      .then((results: User[]) => {
        expect(results.length).toBe(testUsers.length - 1);
      })
      .then(done)
      .catch(done.fail);
  });
});
