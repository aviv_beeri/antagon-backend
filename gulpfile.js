const spawn = require('child_process').spawn;
const gulp = require('gulp');
const ts = require('gulp-typescript');
const eslint = require('gulp-eslint');
const nsp = require('gulp-nsp');
const sourcemaps = require('gulp-sourcemaps');
const tslint = require('gulp-tslint');
const tsProject = ts.createProject('tsconfig.json');

let node = null;
const OUTPUTDIR = 'build';

process.on('exit', (code) => {
  if (node) {
    node.kill();
  }
});

gulp.task('nsp', (cb) => {
  nsp({
    package: `${__dirname}/package.json`
  }, cb);
});

gulp.task('eslint', () => {
  return gulp.src(['src/**/*.ts', '!src/**/*.d.ts'])
    .pipe(eslint({
      extensions: ['.ts', '.js']
    }))
    .pipe(eslint.format());
});

gulp.task('tslint', () => {
  return gulp.src(['src/**/*.ts', '!src/**/*.d.ts'])
    .pipe(tslint({
      formatter: 'prose'
    }))
    .pipe(tslint.report({
      emitError: false
    }));
});

gulp.task('lint', ['tslint', 'eslint']);

gulp.task('check', ['lint', 'nsp']);

gulp.task('watch', () => {
  gulp.watch(['src/**/*.ts'], ['check', 'typescript-build', 'start-app']);
});

gulp.task('typescript-build', () => {
  return gulp.src('src/**/*.ts')
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(OUTPUTDIR));
});

gulp.task('start-app', ['check', 'typescript-build'], () => {
  if (node) {
    node.kill();
  }
  node = spawn('node', [`${OUTPUTDIR}/index.js`], { stdio: 'inherit' });
  node.on('close', (code) => {
    if (code === 0) {
      process.exit();
    } else {
      console.log(`Process closed with code: ${code}`);
      console.log('Waiting for changes...');
    }
  });
});

gulp.task('start', ['start-app', 'watch']);

gulp.task('default', ['start']);
gulp.task('build', ['check', 'typescript-build']);
