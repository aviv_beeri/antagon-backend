FROM mhart/alpine-node:6
# Set environment variables
ENV appDir /var/www/app/current
RUN mkdir -p /var/www/app/current

RUN apk update
RUN apk add git

WORKDIR ${appDir}

# Add our package.json and install *before* adding our application files
ADD package.json ./
RUN npm i --production

ADD . /var/www/app/current
EXPOSE 8080

CMD ["npm", "run", "gulp", "start-app"]
